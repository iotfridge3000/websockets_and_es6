var webpack = require("webpack");
var path = require("path");

module.exports = {
    entry: './components/entry.jsx',
    output: {
        path:path.resolve(__dirname, "public/js"),
        filename:'bundle.js'
    },
    module: {
  	    loaders:[
  		    {
    		    test: /\.jsx?$/,
    		    exclude: /(node_modules|bower_components)/,
    		    loader: 'babel-loader', // 'babel-loader' is also a legal name to reference 
    		    query: {
    		        presets: ['react', 'es2015']
    		    }
		    }
  	    ]
    }
}
