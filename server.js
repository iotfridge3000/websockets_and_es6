var express = require('express');

let app = express();
app.use(express.static('public'));

var http = require('http').Server(app);
var io = require('socket.io')(http);
var _ = require('lodash');
var fs = require('fs');

//have 2 namespaces. One for chat, the other for DOM/Canvas manipulation
//use entire separate ES6 modules as namespaces.
// import * as canvasModule from "./canvasModule.js"
// import * as chatModule from "./chatModule.js"

app.get('/', function(req,res){
    res.sendFile(__dirname + '/views/index.html');
});

app.get('/flexbox', function(req,res){
    res.sendFile(__dirname + '/views/index_flexbox.html');
});

//do a check periodically to check that all socket.id's are mapped a name in this obj?
//make an ES6 Set() out of the nicknames to make sure there are no duplicates
//update nickname mapping to be a room-level feature
let idNicknameMapping = {};

io.on('connection', function(socket){
//convention:
//inbound socket events to the server: present tense
//outbound socket events to the client: past tense

  //assigning nickname to user by default
    idNicknameMapping[socket.id] = "User" + Math.ceil(Math.random() * 1000);

    //joining the default room first
    socket.join("default room");
    // console.log("rooms", socket.rooms);

  //listing current users by their nicknames, NOT socketId
    var userList = Object.keys(socket.conn.server.clients); 

    io.emit('currentUsers', userList);

    io.on('error', function(socket){
        console.log("Shit broke, genius!");
      //dump error to Morgan/Bunyan or some other logging platform
    });

    socket
        .on('chat message', (roomId, msg) => {
            console.log("roomId:", roomId);
            let tokenized = msg.split(" ");
            if (tokenized[0] === "!nick"){
                idNicknameMapping[socket.id] = tokenized[1];
                io.emit('name:change');
            }
            if (msg === "!roll"){
                var diceToss = Math.ceil(Math.random()*20);
                let msg = `Somebody threw a d20 and rolled a ${diceToss}`;
            }
            io.in(roomId).emit('chat message',idNicknameMapping[socket.id], msg, roomId);
        })
        .on('name:change', (msg) => {
          io.emit(oldNick + "changed name to " + newNick);
        })
        .on('user:change:room', function(roomNum){
          socket.join('room'+roomNum);
          socket.emit("room:changed",`{user} has left for room {roomNum}`);
        })
        .on('user:typing', function(){
          io.emit('whois:typing', socket.id);
        })
        .on('user:join:room', function(roomNum){
            socket.join('room'+roomNum);
            socket.emit("room:joined",roomNum,"{user} has left for room {roomNum}");
        })
        .on('user:leave', function(){
            io.in(roomId).emit("user:left", "User " + userName + " has disconnected");
        })
        .on('error', function(){
            console.log("error. Oh no!");
        });

});

http.listen(3000, function(){
  console.log("listening on port 3000");
});
