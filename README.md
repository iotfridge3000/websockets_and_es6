Changing this project to be a Node.js/Golang hybrid project.

Diagram goes here.

Node.js
=======
The admin interface is also viewable through Node.js.

Golang
======
There's a Websocket server here written in Golang. 

Client
======
I've tried to implement the clients to be a bit more intelligent about how they interact with both the Node.js and Golang websocket servers. They will attempt to connect to both upon client start/pageload, but controls on the UI are provided to shut down the reception of events from either source.


